#    factlog-automotive-part-mnf-opt (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-automotive-part-mnf-opt is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

FROM registry.gitlab.com/eltrun-adopt/cplex as cplex
ENV RB_HOST $RB_HOST
ENV RB_PORT $RB_PORT
ENV RB_USER $RB_USER
ENV RB_PASS $RB_PASS
ENV CPLEX_PATH $CPLEX_PATH

FROM python:3.7-slim
COPY --from=cplex /opt/ /opt
ENV PATH="/opt/ibm/ILOG/CPLEX_Studio201/cplex/bin/x86-64_linux:${PATH}"
ENV PYTHONPATH="/opt/ibm/ILOG/CPLEX_Studio201/python/docplex/"

WORKDIR /cont
COPY ./ ./
RUN pip install -r requirements.txt
ENTRYPOINT python main.py $RB_HOST $RB_PORT $RB_USER $RB_PASS $CPLEX_PATH