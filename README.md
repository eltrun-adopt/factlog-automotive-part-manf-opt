# factlog: Automotive Part Manufacturing Optimization


## Description
This code can be applied to a discrete automotive part manufacturing environment that is
modelled as a 2-stage assembly flow shop with resource constraints. Key challenge is the
integration of maintenance planning and scheduling together with the scheduling of
production orders at the production lines. To that end, an analytics module is providing
maintenance windows and the goal is to schedule maintenance activities during periods that
will have a minimum impact on the schedule in terms of makespan and tardiness. Another
capability that is provided is dynamic re-scheduling to capture new urgent orders and
unscheduled machine breakdowns. A rigorous Constraint Programming formulation is
proposed for modeling and solving the problem. Preliminary results on benchmark data sets
validate the applicability of the model and demonstrate the efficiency, effectiveness and
scalability of the proposed CP approach.

## Authors
People who have contributed to this project are Dr Gregory Kasapidis and Dr. Panagiotis Repoussis.

## Acknowledgement
This research was funded by the H2020 FACTLOG project, which has received funding
from the European Union’s Horizon 2020 programme under grant agreement No. 869951.
<https://www.factlog.eu/>

## License
factlog-automotive-part-mnf-opt (c) by the Athens University of Economics and Business, Greece.

factlog-automotive-part-mnf-opt is licensed under a
Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

You should have received a copy of the license along with this
work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.