#    factlog-automotive-part-mnf-opt (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-automotive-part-mnf-opt is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

from datetime import datetime
from docplex.cp.config import DEFAULT_VALUES
from docplex.cp.model import CpoModel
import sys
import os
from dataParser import DataManager
from gant_charter import plotter
from schedule_validator import validate
import json

supress_output = False

def log(*args):
    concat_msg = " ".join(map(str, args))
    if (not supress_output):
        print(datetime.now(), concat_msg)


class SolWorkplace:
    def __init__(self):
        self.id = 0
        self.name = ""
        self.activities = []
        self.maintenanceActivities = []

class SolLine:
    def __init__(self):
        self.id = 0
        self.name = ""
        self.workplaces = {}

class SolResource:
    def __init__(self):
        self.id = 0
        self.name = ""
        self.consumption_segments = []

class Solution:
    def __init__(self):
        self.makespan = 0
        self.total_idle_time = 0
        self.total_tardiness = 0
        self.total_flow_time = 0
        self.gant_chart = None
        self.activities = []
        self.lines = {}
        #Resources
        self.products = {}
        self.resources = {}

    def SaveToJson(self, name="output.json"):
        
        data_dict = {}
        data_dict["Metrics"] = {}
        data_dict["Metrics"]["Makespan"] = self.makespan
        data_dict["Metrics"]["TotalTardiness"] = self.total_tardiness
        data_dict["Metrics"]["TotalFlowTime"] = self.total_flow_time
        data_dict["Metrics"]["AvgTardiness"] = float(self.total_tardiness) / len(set([a.order_id for a in self.activities]))
        data_dict["Metrics"]["TotalWorkStationIdleTime"] = self.total_idle_time
        data_dict["Metrics"]["AvgWorkStationIdleTime"] = float(self.total_idle_time / sum([len(self.lines[l].workplaces) for l in self.lines]))
        data_dict["Schedule"] = []
        
       
        #Sort activities by the order_id
        self.activities.sort(key=lambda x: x.order_id)

        for act in self.activities:
            e = {}
            e["OrderId"] = act.order_id
            e["ProductId"] = act.product_id
            e["LineID"] = act.line_id
            e["WorkplaceID"] = act.workplace_id
            e["WorkplaceTypeID"] = act.workplace_type_id
            e["Start"] = act.start
            e["End"] = act.end

            data_dict["Schedule"].append(e)
        

        json_object = json.dumps(data_dict)
        with open(name, "w") as f:
            #Save dict to json
            f.write(json_object)

        return data_dict


class Activity:
    def __init__(self):
        self.start = 0
        self.order_id = -1
        self.duration = 0
        self.end = 0

def solve(jsondata, solverPath = "/usr/bin/cpoptimizer"):
    #-------------------
    # Helper Functions
    #-------------------

    def issueProductionOrder(model, mgr, order_id, product_id, quantity):
        product = mgr.products[product_id]
        log("Issuing production order for product", product_id, "Quantity", quantity,
        "SourceLineType", product.SourceLineTypesId,
        "EndLineType", product.EndLineTypesId)
        
        if order_id not in order_interval_vars:
            order_interval_vars[order_id] = {}
        
        line_types_list = list(mgr.floor.productionLineTypes)
        for i in range(mgr.floor.productionLineTypes[product.SourceLineTypesId].LocalIndex,
                       mgr.floor.productionLineTypes[product.EndLineTypesId].LocalIndex + 1):
            
            lt = line_types_list[i]
            if i not in order_interval_vars[order_id]:  
                order_interval_vars[order_id][lt] = {}
            
            #For now since we have a single line the line types map 1 on 1 with the actual lines
            #therefore we immediately create one interval variable per workplace
            for line in mgr.floor.lines.values():
                if (line.line_type.Id == lt):
                    for wp in line.workplaces:
                        proc_time = product.processing_times[wp.workplace_type.Id]
                        var = model.interval_var(size=proc_time * quantity)
                        
                        #Explicitly handle final activities
                        if (wp.isLast):
                            set_of_final_vars.append(var)
                        
                        #Keep the interval variable
                        if (product_id not in order_interval_vars[order_id][lt]):
                            order_interval_vars[order_id][lt][product_id] = {}
                        
                        #Resource Accumulation
                        #Request all bom resources at the first workplace of the line
                        if (wp.isFirst):
                            for bome in mgr.products[product_id].required_products:
                                product_cumul_functions[bome.required_material.Id] -= mdl.step_at_start(var, quantity * bome.multiplicity)
                                
                            for bome in mgr.products[product_id].required_resources:
                                resource_cumul_functions[bome.required_material.Id] -= mdl.step_at_start(var, quantity * bome.multiplicity)
                        
                        #Add production quantity at the end of the final variable
                        #TESTING: Do not make the final produced quantity available in the system. This will be reserved for serving the order
                        if (wp.isLast and product_id != mgr.orderMap[order_id].product.Id):
                            product_cumul_functions[product_id] += mdl.step_at_end(var, quantity)
                        
                        order_interval_vars[order_id][lt][product_id][wp.Id] = var #Save Interval variable
                    break


    def populateProductInitInventory(prod):
        if (prod.Id not in product_cumul_functions):
            product_cumul_functions[prod.Id] = mdl.step_at(0, prod.initial_inventory)
            prod.consumption_segments.append((0, prod.initial_inventory))
            product_temp_init_inventory[prod.Id] = prod.initial_inventory
        
        for bome in prod.required_products:
            populateProductInitInventory(bome.required_material)
    
    mgr = DataManager()
    try:
        mgr.parseDataFromJson(jsondata)
    except Exception as ex:
        log("Malformed input data", ex)
        return None

    # Create CPO model
    mdl = CpoModel()

    #Create interval variable per order
    order_interval_vars = {}
    workplace_sequence_vars ={}
    maintenance_workplace_interval_vars = {}
    resource_cumul_functions = {}
    product_cumul_functions = {}
    set_of_final_vars = [] #End operations
    product_temp_init_inventory = {}
    product_order_quantities = {}

    #Product accumulation functions Init
    for o in mgr.orders:
        populateProductInitInventory(o.product)


    #Component accumulation functions Init
    for rc in mgr.resources.values():
        resource_cumul_functions[rc.Id] = mdl.step_at(0, rc.initial_inventory)
        rc.consumption_segments.append((0, rc.initial_inventory))


    #TODO: THIS IS INTENDED FOR RESOURCE TESTING

    #Injecting resources every hour
    for rc in mgr.resources.values():

    #Initialize maintenance activity list per workplace
    for line in mgr.floor.lines.values():
        for wp in line.workplaces:
            maintenance_workplace_interval_vars[wp.Id] = []

    #-----------------------------------------------------------------------------
    # Build the model
    #-----------------------------------------------------------------------------

    for o in mgr.orders:
        actual_order_quantity = o.Quantity - product_temp_init_inventory[o.product.Id]
        #TODO: Split orders into suborders based on the quantity
        log("Generating jobs for order", o.Id, "product", o.product.Id, "quantity", actual_order_quantity)

        #Fetch ProductBOM information for the product
        for bome in o.product.required_products:
            bom_prod_quantity = actual_order_quantity * bome.multiplicity - product_temp_init_inventory[bome.required_material.Id]
            if (bom_prod_quantity > 0):
                issueProductionOrder(mdl, mgr, o.Id, bome.required_material.Id, bom_prod_quantity)
                product_temp_init_inventory[bome.required_material.Id] -= bom_prod_quantity #A part of the initial inventory was used
                product_temp_init_inventory[bome.required_material.Id] = max(product_temp_init_inventory[bome.required_material.Id] , 0)
        issueProductionOrder(mdl, mgr, o.Id, o.product.Id, actual_order_quantity)

    #Model Constraints
    
    #Add workplace precedencies
    for o in mgr.orders:
        #Iterate in linetypes
        for lt in order_interval_vars[o.Id]:
            #Iterate in lines:
            for line in mgr.floor.lines.values():
                if (line.line_type.Id != lt):
                    continue
                
                #Iterate in products
                for pt in order_interval_vars[o.Id][lt]:
                    #Iterate in workplaces
                    for wp1 in line.workplaces:
                        for wp2 in line.workplaces:
                            if (wp2.SequenceInLine > wp1.SequenceInLine):
                                mdl.add(mdl.end_before_start(order_interval_vars[o.Id][lt][pt][wp1.Id], order_interval_vars[o.Id][lt][pt][wp2.Id]))        


    for o in mgr.orders:
        p_vars = []
        f_vars = []
        
        #last preassembly vars
        for line in mgr.floor.lines.values():
            if (line.line_type.Code == "PRA" and line.line_type.Id in order_interval_vars[o.Id]):
                for pt in order_interval_vars[o.Id][line.line_type.Id]:
                    lastwp = [wp for wp in line.workplaces if wp.isLast == True][0]
                    p_vars.append(order_interval_vars[o.Id][line.line_type.Id][pt][lastwp.Id])

        #first final assembly vars
        for line in mgr.floor.lines.values():
            if (line.line_type.Code == "FA" and line.line_type.Id in order_interval_vars[o.Id]):
                for pt in order_interval_vars[o.Id][line.line_type.Id]:
                    lastwp = [wp for wp in line.workplaces if wp.isFirst == True][0]
                    f_vars.append(order_interval_vars[o.Id][line.line_type.Id][pt][lastwp.Id])


        #Add constraints
        for v1 in p_vars:
            for f1 in f_vars:
                mdl.add(mdl.end_before_start(v1, f1))        

    
    #Add maintenenance activities along with their constraints
    for mact_id in range(len(mgr.maintenanceScheduledActivities)):
        mact = mgr.maintenanceScheduledActivities[mact_id]
        line = mgr.floor.workplaces[mact.workplace_id].line
        
    #No Overlap Constraints
    d = order_interval_vars
    for lt in mgr.floor.productionLineTypes:
        #Create Order Setup Time Transition Matrix
        setup_time_matrix = []

        #Init setup time matrix
        for i in range(len(mgr.products) + 1):
            t = [0]* (len(mgr.products) + 1)
            setup_time_matrix.append(t)
        
        for i in mgr.products:
            family_i_id = mgr.products[i].ProductFamiliesId
            family_i = mgr.productFamilies[family_i_id]
            
            #Setup time with dummy
            if (lt in family_i.setup_times):
                    setup_time_matrix[len(mgr.products)][mgr.products[i].LocalIndex] = family_i.setup_times[lt]
            
            for j in mgr.products:
                if (i == j):
                    continue
                
                family_j_id = mgr.products[j].ProductFamiliesId
                
                if (lt in family_i.setup_times and family_i_id != family_j_id):
                    setup_time_matrix[mgr.products[j].LocalIndex][mgr.products[i].LocalIndex] = family_i.setup_times[lt]
                
        
        #Iterate in lines:
        for line in mgr.floor.lines.values():
            if (line.line_type.Id != lt):
                continue
            
            for wp in line.workplaces:
                #Define Sequence Variable
                variables = []
                variable_types = []
                
                #Add a dummy operation first
                dummy_var = mdl.interval_var(size=0)
                variables.append(dummy_var)
                variable_types.append(len(mgr.products))

                #Add machine maintenance activities for this workplace
                for mact in maintenance_workplace_interval_vars[wp.Id]:
                    variables.append(mact)
                    variable_types.append(len(mgr.products))

                for o_id in d:
                    
                    if lt not in d[o_id]:
                        continue
                    
                    for p_id in d[o_id][lt]:
                        variables.append(d[o_id][lt][p_id][wp.Id])
                        variable_types.append(mgr.products[p_id].LocalIndex)
                
                s_var = mdl.sequence_var(variables, types=variable_types, name="seq_"+str(lt)+"_"+str(wp.Id))
                
                #Force the dummy as the first operation of the sequence
                mdl.add(mdl.first(s_var, dummy_var))

                #Save Sequence Variable
                workplace_sequence_vars[wp.Id] = s_var
                mdl.add(mdl.no_overlap(s_var, setup_time_matrix))
            
            #Make sure to maintain the same subsequence for all workplaces of the same line
            for i in range(len(line.workplaces) - 1):
                for j in range(i+1, len(line.workplaces)):
                    wp1 = line.workplaces[i]
                    wp2 = line.workplaces[j]
                    mdl.add(mdl.same_sequence(workplace_sequence_vars[wp1.Id],
                                              workplace_sequence_vars[wp2.Id]))
            
    #Resource Constraints
    #Make sure component availability is always > 0
    for rc in mgr.resources.values():
        #mdl.add(resource_cumul_functions[rc.Id] >= 0) #Enable when resource constraints should be considered
        pass

    #Make sure product availability is always >0
    for prod_id in product_temp_init_inventory:
        mdl.add(product_cumul_functions[prod_id] >= 0)
    
    #Makespan Objective
    mdl.add(mdl.minimize(mdl.max(mdl.end_of(v) for v in set_of_final_vars)))
    #mdl.export_as_cpo("export_model.out")
    #TODO Add extra objectives for the minimization of the machine idle time and the tardiness

    # Solve model
    log("\nSolving model....")
    
    msol = mdl.solve(TimeLimit=10, agent='local',
                    execfile=solverPath, LogVerbosity="Terse")
    
    #Report + Construct Solution

    s = Solution()
    s.makespan = 0
    s.total_tardiness = 0
    s.total_idle_time = 0
    s.total_flow_time = 0

    log("Solution Objective:", s.makespan)

    for i in mgr.floor.lines:
        l = SolLine()
        l.name = mgr.floor.lines[i].Code
        l.id = i
        lt = mgr.floor.lines[i].line_type
        for wp in mgr.floor.lines[i].workplaces:
            w = SolWorkplace()
            w.id = wp.Id
            w.name = wp.Code
            wp_first_time = 100000000000000
            wp_last_time = 0
            wp_active_time = 0
            for o in mgr.orders:
                if lt.Id not in order_interval_vars[o.Id]:
                    continue
                for pt in order_interval_vars[o.Id][lt.Id]:
                    #Generate Activity
                    act = Activity()
                    act.order_id = o.Id
                    act.product_id = pt
                    act.line_id = i
                    act.workplace_id = wp.Id
                    act.workplace_type_id = wp.workplace_type.Id
                    act.start = msol[order_interval_vars[o.Id][lt.Id][pt][wp.Id]][0]
                    act.end = msol[order_interval_vars[o.Id][lt.Id][pt][wp.Id]][1]
                    act.duration = act.end - act.start
                    act.name = "Order " + str(o.Id)
                    w.activities.append(act)
                    s.activities.append(act) #Save activity to raw solution activity list
                    log("Order ", o.Id, "Product", pt, "Line", i, "Workplace", wp.Id, "Type", wp.workplace_type.Id,
                    "Start", act.start, "End", act.end)
                    wp_last_time = max(wp_last_time, act.end)
                    wp_active_time += act.duration
                    wp_first_time = min(wp_first_time, act.start)
                    s.makespan = max(s.makespan, act.end)
            s.total_idle_time += wp_last_time - wp_active_time - wp_first_time
            s.total_flow_time += wp_active_time
            

            l.workplaces[w.id] = w
        s.lines[l.id]= l
    
    #Calculate tardiness
    for o in mgr.orders:
        o_last_time = 0
        for act in s.activities:
            if (act.order_id == o.Id):
                o_last_time = max(o_last_time, act.end)
        s.total_tardiness += max(0, o_last_time - o.due_date)
    
    #Maintenance Activities
    for i in mgr.floor.lines:
        for wp in mgr.floor.lines[i].workplaces:
            w = s.lines[i].workplaces[wp.Id]
            for v in maintenance_workplace_interval_vars[wp.Id]:
                #Generate Activity
                act = Activity()
                try:
                    act.start = msol[v][0]
                    act.end = msol[v][1]
                    act.duration = act.end - act.start
                    act.name = "M"
                    log("Maintenance Activity in Line ", i, "Workplace", wp.Id, "Type", wp.workplace_type.Id,
                    "Start", msol[v][0],"End", msol[v][1])
                except:
                    continue #Maintenance Activity not included in the solution
                w.maintenanceActivities.append(act)




    #Try to fetch resource segments
    #Need to recalculate everything based on the final interval variables

    #Iterate in orders
    for o in mgr.orders:
        for l in mgr.floor.lines.values():
            if not (l.line_type.Id in order_interval_vars[o.Id]):
                continue
            
            #Iterate on all product entries but for now there should be only one match
            for prod in order_interval_vars[o.Id][l.line_type.Id]:
                for wp in l.workplaces:
                    if (wp.isFirst):
                        #Load Interval Variable
                        var = order_interval_vars[o.Id][l.line_type.Id][prod][wp.Id]
                        
                        #Get start time of interval variable in solution
                        var_start = msol[var][0]

                        #Add consumption to required resources
                        for bome in mgr.products[prod].required_products:
                            prodrc = mgr.products[bome.required_material.Id]
                            prodrc.consumption_segments.append((var_start, -bome.multiplicity * o.Quantity))

                        for bome in mgr.products[prod].required_resources:
                            rc = mgr.resources[bome.required_material.Id]
                            rc.consumption_segments.append((var_start, -bome.multiplicity * o.Quantity))
                    
                    elif (wp.isLast):
                        #Load Interval Variable
                        var = order_interval_vars[o.Id][l.line_type.Id][prod][wp.Id]
                        
                        #Get end time of interval variable in solution
                        var_end = msol[var][1]

                        #Add production 
                        mgr.products[prod].consumption_segments.append((var_end, o.Quantity))



    #Report Resources
    for rc in mgr.resources.values():
        src = SolResource()
        src.id = rc.Id
        src.name = rc.Name
        log("Resource", rc.Id, "Segments")
        src.consumption_segments = sorted(rc.consumption_segments, key=lambda x: x[0])
        for seg in src.consumption_segments:
            log(seg)
        s.resources[src.id] = src


    for prodrc in mgr.products.values():
        src = SolResource()
        src.id = prodrc.Id
        src.name = prodrc.Name
        log("Product", prodrc.Id, prodrc.Name, "Segments")
        src.consumption_segments = sorted(prodrc.consumption_segments, key=lambda x: x[0]) 
        for seg in src.consumption_segments:
            log(seg)
        s.products[src.id] = src

    #Plot Gannt Chart of Solution
    #plotter(s)

    return s