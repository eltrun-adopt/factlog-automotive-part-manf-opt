#    factlog-automotive-part-mnf-opt (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-automotive-part-mnf-opt is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
from datetime import datetime
from operator import indexOf

class ShopFloor:
    def __init__(self):
        self.lineNumber = 0
        self.lines = {} # the key should be the lineID the value should the line object
        self.workplaces = {} # the key should be the workplaceID the value should the workplace object
        self.productionLineTypes = {} #the key should be the linetypeID value should be objects of type LineTYpe
        self.workplaceTypes = {} #the key should be the workplacetypeid value should be objects of type WorkplaceType
        self.storageZones = {} #the key should be the storageZoneID value should be objects of type StorageZone

    def report(self):
        print("Floor Workplace Types")
        for e in self.workplaceTypes.values():
            print (e.Id, e.Code)
            

class ProductionLineType:
    def __init__(self, Id = 0, Code = "F", Description =""):
        self.Id = Id
        self.Code = Code
        self.Description = Description


class ProductionLine:
    def __init__(self, Id = 0, Code = "", Description = "", *args, **kwargs):
        self.Id = Id
        self.Code = Code
        self.Description = Description
        self.line_type = None
        self.workplaces = [] #Workplaces should be in sequence

    def sortWokplaces(self):
        pass


class WorkplaceType:
    def __init__(self, Id=0, Code="", Description=""):
        self.Id = Id
        self.Code = Code
        self.Description = Description


class Workplace:
    def __init__(self, Id = 0, Code = "", Description = "", SequenceInLine = -1, *args, **kwargs):
        self.Id = Id
        self.Code = Code
        self.Description = Description
        self.workplace_type = None
        self.line = None
        self.SequenceInLine = SequenceInLine
        self.isLast = False
        self.isFirst = False

class BOMEntry:
    def __init__(self):
        self.required_material = None
        self.workplace_id = 0
        self.multiplicity = 0


class StorageZone:
    def __init__(self):
        self.id = 0
        self.name = ""
        self.capacity = 100000000
        
class ProcessingTimeEntry:
    def __init__(self):
        self.workplace_type_id = 0
        self.ideal_processing_time = 0
        self.real_processing_time = 0

class Product:
    def __init__(self, Id = 0, LocalId = 0,
                       Name="", 
                       Description ="", 
                       EfficiencyRate=0.0,
                       ProductFamiliesId= -1,
                       SourceLineTypesId = -1,
                       EndLineTypesId = -1):
        self.Id = Id
        self.Name = Name
        self.Description = Description
        self.EfficiencyRate = EfficiencyRate
        self.ProductFamiliesId = ProductFamiliesId
        self.SourceLineTypesId = SourceLineTypesId
        self.EndLineTypesId = EndLineTypesId
        
        #Extra data
        self.initial_inventory = 0
        self.required_resources = [] #Bom Entries for Resources
        self.required_products = [] #Bom Entries for Intermediate Products
        self.processing_times = {} #Key should be the workplacetypeID, value should be the effective processing type
        self.consumption_segments = []

class Resource:
    def __init__(self, Id = 0, Name = "", Description = ""):
        self.Id = Id
        self.Name = Name
        self.Description = Description
        self.initial_inventory = 0
        self.consumption_segments = []


class ProductFamily:
    def __init__(self, Id=0, Name="", Description=""):
        self.Id = Id
        self.Name = Name
        self.Description = Description
        self.setup_times = {} #key is the linetypeID, value is the setup time required


class Order:
    def __init__(self, Id=0, Name="", Quantity=0,
                       Priority=0, MaxQuantity=0, ProductsId=-1, *args, **kwargs):
        self.Id = Id
        self.Name = Name
        self.Quantity = Quantity
        self.MaxQuantity = MaxQuantity
        self.Priority = Priority
        self.product = None
        self.due_date = 0 #Due date in seconds from the start of the schedule

class MaintenanceActivity:
    def __init__(self, Duration = 0, 
                       WorkplacesId = 0, **kwargs):
        self.workplace_id = WorkplacesId
        self.start = 0
        self.end = 0
        self.duration = Duration


class DataManager:
    def __init__(self):
        self.floor = ShopFloor()
        self.productFamilies = {}
        self.products = {}
        self.resources = {}
        self.maintenanceScheduledActivities = []
        self.maintenanceUnscheduledActivities = []
        self.orders = []
        self.orderMap = {}
    
    def report(self):
        self.floor.report()

        #Report Orders
        print("Order Report")
        for o in self.orders:
            print(o.Id, o.Name, o.product.Id, o.Quantity)

        print("Scheduled Maintenance Activity Report")
        for o in self.maintenanceScheduledActivities:
            print(o.workplace_id, o.start, o.duration)

        print("Unscheduled Maintenance Activity Report")
        for o in self.maintenanceUnscheduledActivities:
            print(o.workplace_id, o.start, o.duration)
    
    def parseDataFromJson(self, data):
        
        
        schedulingtime = datetime.now() 
        
        dataFields = [
            "WorkplaceTypes",
            "LineTypes",
            "Lines",
            "Workplaces",
            "ProductFamilies",
            "Products",
            "Resources",
            "ResourceBOM",
            "ProductBOM",
            "SetupTimes",
            "ProcessingTimes"
        ]

        #Check essential fields
        for field in dataFields:
            try:
                d = data["staticData"][field]
            except:
                raise Exception("Missing Field " + field + " from input")
        
        #Parse Static Data
        #WorkPlaceTypes
        for e in data["staticData"]["WorkplaceTypes"]:
            wp = WorkplaceType(**e)
            self.floor.workplaceTypes[wp.Id] = wp

        #LineTypes
        for e in data["staticData"]["LineTypes"]:
            lt = ProductionLineType(**e)
            lt.LocalIndex = len(self.floor.productionLineTypes) 
            self.floor.productionLineTypes[lt.Id] = lt
        
        #Lines
        for e in data["staticData"]["Lines"]:
            l = ProductionLine(**e)
            l.line_type = self.floor.productionLineTypes[e["LineTypesId"]]
            self.floor.lines[l.Id] = l

        #Workplaces
        for e in data["staticData"]["Workplaces"]:
            wp = Workplace(**e)
            wp.workplace_type = self.floor.workplaceTypes[e["WorkplaceTypesId"]]
            wp.line = self.floor.lines[e["LinesId"]]
            self.floor.lines[wp.line.Id].workplaces.append(wp)
            self.floor.workplaces[wp.Id] = wp
            
        #Sort line workplaces based on their sequence ID
        for line in self.floor.lines:
            self.floor.lines[line].workplaces.sort(key=lambda x: x.SequenceInLine)
            #Mark first and last workplaces of the line
            self.floor.lines[line].workplaces[0].isFirst = True
            self.floor.lines[line].workplaces[-1].isLast = True

        
        #Storage Zones
        sz = StorageZone()
        sz.id = 1
        sz.name = "Main WIP"
        sz.capacity = 100000
        self.floor.storageZones[sz.id] = sz
        
        #Product Families
        for e in data["staticData"]["ProductFamilies"]:
            pf = ProductFamily(**e)
            self.productFamilies[pf.Id] = pf

        #Products
        for e in data["staticData"]["Products"]:
            pt = Product(**e)
            #Set Local Product Index used for a more efficient product indexing
            pt.LocalIndex = len(self.products) 
            self.products[pt.Id] = pt
        
        #Resources + ResourceBOM
        #No resources are provided for now, the factory assumes that
        #there is plenty to work with
        
        #Testing with custom Resource BOM

        #Resources
        for e in data["staticData"]["Resources"]:
            rc = Resource(**e)
            self.resources[rc.Id] = rc
        
        #Resource BOM
        for e in data["staticData"]["ResourceBOM"]:
            pt1 = self.products[e["ProductsId"]]
            pt2 = self.resources[e["ResourceID"]]
            
            bome = BOMEntry()
            bome.required_material = pt2
            bome.workplace_id = e["WorkplacesId"]
            bome.multiplicity = e["Multiplicity"]
            pt1.required_resources.append(bome)

        
        #ProductBOM
        for e in data["staticData"]["ProductBOM"]:
            pt1 = self.products[e["ProductsId"]]
            pt2 = self.products[e["RequiredProductsId"]]
            
            bome = BOMEntry()
            bome.required_material = pt2
            bome.workplace_id = e["WorkplacesId"]
            bome.multiplicity = e["Multiplicity"]
            pt1.required_products.append(bome)
        
        
        #SetupTimes
        for e in data["staticData"]["SetupTimes"]:
            pf = self.productFamilies[e["ProductFamiliesId"]]
            pf.setup_times[e["LineTypesId"]] = e["SetupTime"]

        #ProcessingTimes
        for e in data["staticData"]["ProcessingTimes"]:
            mat = self.products[e["ProductsId"]]
            #Use Ideal ProcessingTimes
            #NOTE: According to radu processing times are random
            #between 30 - 1800. The variance is high so I'm assuming that they are miliseconds
            #mat.processing_times[e["WorkplaceTypesId"]] = max(int(e["IdealProcessingTime"] * 0.001), 1)
            #Use Real ProcessingTimes
            mat.processing_times[e["WorkplaceTypesId"]] = e["IdealProcessingTime"]
        
        #Dynamic Data
        
        #Orders
        for e in data["dynamicData"]["ProductionOrders"]:
            order = Order(**e)
            order.product = self.products[e["ProductsId"]]
            
            dt = datetime.strptime(e["DueDate"], "%Y-%m-%dT%H:%M:%S.%f")
            order.due_date = max((dt - schedulingtime).total_seconds(), 0)
            
            self.orders.append(order)
            self.orderMap[order.Id] = order
        
        #Scheduled Maintenance Activities
        for e in data["dynamicData"]["ScheduledMaintenanceActivities"]:
            ma = MaintenanceActivity(**e)
            #TODO multiplier 10 was used for the demo experiments
            ma.duration = int(round(e["Duration"]) * 10)
            
            #Convert start time
            dt = datetime.strptime(e["Start"], "%Y-%m-%dT%H:%M:%S.%f")
            #TODO seconds is used instead of total_seconds
            ma.start = int((dt - schedulingtime).seconds) 
            self.maintenanceScheduledActivities.append(ma) #Start is a time window for the activity
        