#!/usr/bin/python
#    factlog-automotive-part-mnf-opt (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-automotive-part-mnf-opt is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import matplotlib as mplot
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import cm
import matplotlib.patches as patches
import sys
from io import BytesIO

mplot.use('Agg')

MAX_ORDER_NUM = 50

color = iter(cm.hsv(np.linspace(0, 1, MAX_ORDER_NUM)))

order_colors = []
for i in range(MAX_ORDER_NUM):
    order_colors.append(next(color))

# print order_colors
events = {}
machineLineMap = {}


def plotter(s):
    # Setup Plot
    fig, ax = plt.subplots(nrows=len(s.lines), figsize=(30, 15))
    line_counter = 0
    
    #Iterate in lines
    for l in s.lines:
        line = s.lines[l]
        workplaceNum = len(line.workplaces)
        mach_labs_pos = []
        mach_labs = []
        if workplaceNum == 0:
            continue
        
        y = 0
        counter = 0
        for w in line.workplaces:
            wp = line.workplaces[w]
            y = counter * 10
            mach_labs_pos.append(y + 5)
            mach_labs.append("Workplace " + str(wp.id))
            
            #Normal Activities
            for j in range(len(wp.activities)):
                act = wp.activities[j]
                #Add patch
                ax[line_counter].add_patch(patches.Rectangle(
                    (act.start, y),   # (x,y)
                    act.duration,  # width
                    10.0,          # height
                    fill=True,
                    alpha=0.6,
                    linestyle='--',
                    edgecolor="black",
                    facecolor=order_colors[act.order_id % MAX_ORDER_NUM]
                    # hatch='+'
                ))
                tex_cx = act.start + (act.duration) / 2.0
                tex_cy = y + 5.0
                #tex = "Job: " + str(op_job) + " ID: " + str(op_id)
                tex = "Ο#" + str(act.order_id)
                ax[line_counter].annotate(tex, (tex_cx, tex_cy), color='b', weight='bold',
                            fontsize=10, ha='center', va='center')

            #Maintenance Activities
            for j in range(len(wp.maintenanceActivities)):
                act = wp.maintenanceActivities[j]
                #Add patch
                ax[line_counter].add_patch(patches.Rectangle(
                    (act.start, y),   # (x,y)
                    act.duration,  # width
                    10.0,          # height
                    fill=True,
                    alpha=0.6,
                    linestyle='--',
                    edgecolor="black",
                    facecolor='black'
                    # hatch='+'
                ))
                tex_cx = act.start + (act.duration) / 2.0
                tex_cy = y + 5.0
                tex = "ΜΟ#" + str(j + 1)
                ax[line_counter].annotate(tex, (tex_cx, tex_cy), color='b', weight='bold',
                            fontsize=10, ha='center', va='center')
            
            counter += 1
        
        ax[line_counter].set_yticks(mach_labs_pos)
        ax[line_counter].set_yticklabels(mach_labs)
        ax[line_counter].set_xlim([0, s.makespan])
        ax[line_counter].set_ylim([0, counter * 10])
        ax[line_counter].set_ylabel(line.name, rotation=90)
        
        line_counter += 1

    fig.suptitle('GANTT CHART', fontsize=16)
    #plt.legend()

    #Save to memory
    memfigure = BytesIO()
    fig.savefig(memfigure, dpi=300, bbox_index = 'tight', format='png')
    plt.close(fig)

    #Also save figure to solution
    s.gant_chart = memfigure

    #Save to disk as well
    memfigure.seek(0)
    with open("gant.png", 'wb') as f:
        f.write(memfigure.read())    

    #Plot Resources
    fig = plt.figure(figsize=(30, 15))

    for rc in s.resources.values():
        #Accumulate Resource Level
        x = [0]
        y = [0]

        if (len(rc.consumption_segments) <= 1):
            continue
        
        for i in range(len(rc.consumption_segments)):
            seg = rc.consumption_segments[i]
            if (seg[0] == x[-1]):
                y[-1] += seg[1]
            else:
                x.append(seg[0])
                y.append(y[-1] + seg[1])
        
        #Step plot
        xx = [x[0]]
        yy = [y[0]]
        for i in range(1, len(x)):
            xx.append(x[i])
            yy.append(y[i - 1])

            xx.append(x[i])
            yy.append(y[i])
            
        plt.scatter(xx, yy, label=rc.name)
        plt.plot(xx,yy)
    plt.legend()

    
    #Save to memory
    memfigure = BytesIO()
    fig.savefig(memfigure, dpi=300, bbox_index = 'tight', format='png')
    plt.close(fig)

    #Also save figure to solution
    s.resource_plot = memfigure

    #Save to disk as well
    memfigure.seek(0)
    with open("resource_plot.png", 'wb') as f:
        f.write(memfigure.read())    


