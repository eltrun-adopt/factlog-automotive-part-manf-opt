#    factlog-automotive-part-mnf-opt (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-automotive-part-mnf-opt is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import os,sys,pika,json,time
from datetime import datetime

from pika.spec import Channel
from cp_solver import solve

def log(msg):
    print(datetime.now(), msg)


def main(hostname, port, username, password, solverPath, input_queue, output_queue):
    credentials = pika.PlainCredentials(username, password)
    params = pika.ConnectionParameters(hostname, port, '/', credentials, heartbeat=1860, blocked_connection_timeout=930)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    
    def callback(ch, method, properties, body):
        log(" [x] Received Message from queue")
        jsondata = json.loads(body)
        uuid = jsondata["uuid"]
        data = jsondata["data"]

        output = {}
        try:
            solution = solve(data, solverPath) #Call solver
            print("attempt to access s")
            print(dir(solution))
            # outputdata = s.SaveToJson(name="output.json")
            outputdata = solution.SaveToJson()
            print("attempt to access outputdata")
            print(outputdata)
            output = {"uuid" : uuid, "data" : outputdata, "produced_at" : int(time.time() * 1000) }
            log(' [*] Result sent successfully to result queue')        
        except Exception as ex:
            error_message = {"message" : str(ex)}
            output = {"uuid" : uuid, 
                      "data" : error_message, 
                      "produced_at" : int(time.time() * 1000) }
            log(' [*] Error during execution')   
        
        output = json.dumps(output)
        channel.basic_publish(exchange="opt-result", 
                                routing_key="prod-and-maint-sched",
                                body = output)
    
    channel.basic_consume(queue=input_queue, on_message_callback=callback, auto_ack=True)

    log(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        #Fetch connection parameters from cmd
        assert(len(sys.argv) != 5)
        
        hostname = sys.argv[1]
        port = sys.argv[2]
        username = sys.argv[3]
        password = sys.argv[4]
        solverPath = sys.argv[5]
        
        in_queue = "prod-and-maint-sched_job"
        out_queue = "prod-and-maint-sched_result"
        
        main(hostname, port, username, password, solverPath, in_queue, out_queue)
    except KeyboardInterrupt:
        log("Terminating after User's Request")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)