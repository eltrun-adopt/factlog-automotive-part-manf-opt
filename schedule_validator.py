#    factlog-automotive-part-mnf-opt (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-automotive-part-mnf-opt is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

from importlib import resources
from dataParser import DataManager
import json


class Job:
    def __init__(self):
        self.id = -1
        self.order_id = -1
        self.isOrderJob = False
        self.product_id = -1
        self.quantity = 0
        self.operations = []


class Operation:
    def __init__(self):
        self.id = -1
        self.start = 0
        self.end = 0
        self.proc_time = 0
        self.workplace_id = 0
        self.workplace_type_id = 0
        self.sucs = []
        self.preds = []
        self.job = None
        self.scheduled = False

    def isFirst(self):
        return len(self.preds) == 0

    def isLast(self):
        return len(self.sucs) == 0



class ResourceBlock:
    def __init__(self):
        self.start = 0
        self.end = 100000000000
        self.quantity = 0

    def report(self):
        print(self.start, self.end, self.quantity)

class Resource:
    def __init__(self):
        self.res_blocks = [ResourceBlock()]


    def hasQuantity(self, q):
        for i in range(len(self.res_blocks)):
            bi = self.res_blocks[i]
            if (bi.quantity >= q):
                future_blocks_good = True
                for j in range(i + 1, len(self.res_blocks)):
                    bj = self.res_blocks[j]
                    if (bj.quantity < q):
                        future_blocks_good = False
                
                if (future_blocks_good):
                    return bi.start
        return -1
    
    def removeQuantity(self, time, q):
        self.addQuantity(time, -q)

    def removeQuantityOld(self, time, q):
        #Find the block that contains time
        time_block_id = -1
        for b_id in range(len(self.res_blocks) - 1, -1, -1):
            b = self.res_blocks[b_id]
            if (time >= b.start):
                time_block_id = b_id
                break

        current_block = self.res_blocks[time_block_id]
        if (time != current_block.start):
            #Add New Block to split the time window
            new_block = ResourceBlock()
            new_block.start = time
            new_block.end = current_block.end
            new_block.quantity = current_block.quantity
            time_block_id += 1
            self.res_blocks.insert(time_block_id, new_block)
            current_block.end = time

        #Update all future blocks including the new one (if any)
        for i in range(time_block_id, len(self.res_blocks)):
            self.res_blocks[i].quantity -= q

    def addQuantity(self, time, q):
        #Find the block that contains time
        time_block_id = -1
        for b_id in range(len(self.res_blocks) - 1, -1, -1):
            b = self.res_blocks[b_id]
            if (time >= b.start):
                time_block_id = b_id
                break
        
        current_block = self.res_blocks[time_block_id]
        if (time != current_block.start):
            #Add New Block to split the time window
            new_block = ResourceBlock()
            new_block.start = time
            new_block.end = current_block.end
            new_block.quantity = current_block.quantity
            time_block_id += 1
            self.res_blocks.insert(time_block_id, new_block)
            current_block.end = time
        
        #Update all future blocks including the new one (if any)
        for i in range(time_block_id, len(self.res_blocks)):
            self.res_blocks[i].quantity += q
        
    
    def report(self):
        for b in self.res_blocks:
            b.report()

        
def validate(sol_json, data_json):
    
    #Construct datamanager
    mgr = DataManager()
    try:
        mgr.parseDataFromJson(data_json)
    except Exception as ex:
        print("Malformed input data", ex)
        return

    jobs = []
    job_counter = 0
    op_counter = 0
    for order in mgr.orders:
        pa_jobs = []
        fa_jobs = []

        for line in mgr.floor.lines:
            
            #Define Job
            jb = Job()
            jb.id = job_counter
            jb.order_id = order.Id
            jb.quantity = order.Quantity
            
            job_operations = [x for x in sol_json['Schedule'] if x['OrderId']==order.Id and x['LineID'] == line]
            
            if (len(job_operations) == 0):
                continue
            
            if (mgr.floor.lines[line].line_type.Code == "FA"):
                fa_jobs.append(jb)
            else:
                pa_jobs.append(jb)

            #print(job_operations)
            jb.product_id = job_operations[0]["ProductId"]

            if (jb.product_id == order.product.Id):
                jb.isOrderJob = True
            
            #Set Operations
            for wp in mgr.floor.lines[line].workplaces:
                op_entry = next((x for x in job_operations if x["WorkplaceID"] == wp.Id), None)
                
                if (op_entry is None):
                    continue

                #Define Operation
                op = Operation()
                op.id = op_counter
                op.start = op_entry["Start"]
                op.end = op_entry["End"]
                op.workplace_id = op_entry["WorkplaceID"]
                op.workplace_type_id = op_entry["WorkplaceTypeID"]
                op.job = jb
                jb.operations.append(op)
            
                op_counter +=1
            
            job_counter += 1
            jobs.append(jb)

        #Simulation Adaptation
        #Add precedencies between the fa and pa jobs
        for jb1 in pa_jobs:
            for jb2 in fa_jobs:
                op1 = jb1.operations[-1]
                op2 = jb2.operations[0]
                
                op1.sucs.append(op2)
                op2.preds.append(op1)

    #Report Jobs and operations
    # for jb in jobs:
    #     for op in jb.operations:
    #         print(jb.id, jb.order_id, jb.product_id, op.start, op.end, op.workplace_id)

    #Init workplace lists
    workplaces = {}
    for wp in mgr.floor.workplaces:
        workplaces[wp] = []
    
    
    #Store operations in the appropriate workplace
    for jb in jobs:
        for op in jb.operations:
            workplaces[op.workplace_id].append(op)

    #Set correct operation precedencies
    for jb in jobs:
        for i in range(len(jb.operations) - 1):
            op1 = jb.operations[i]
            op2 = jb.operations[i + 1]

            op1.sucs.append(op2)
            op2.preds.append(op1)


    #Identify the correct scheduling sequence based on the calculated start times
    for wp in workplaces:
        t = sorted(workplaces[wp], key=lambda x: x.start)
        workplaces[wp] = t
        for op in workplaces[wp]:
            #Clear start, end times and calculate proc time for the operation
            op.proc_time = op.job.quantity * mgr.products[op.job.product_id].processing_times[op.workplace_type_id]
            
            #Crosscheck processing time
            if (op.proc_time != op.end - op.start):
                print("PROCESSING TIME CALCULATION ERROR")

            op.start = 0
            op.end = 0

    #Identify final product quantity requirements
    order_requirements = {}
    for o in mgr.orders:
        if (o.product.Id not in order_requirements.keys()):
            order_requirements[o.product.Id] = 0
        order_requirements[o.product.Id]+= o.Quantity
    
    #Init Resource Accumulation Arrays
    resource_state = {}
    produced_resources = {}
    for jb in jobs:
        resource_state[jb.product_id] = Resource()
        produced_resources[jb.product_id] = 0
    
    #Reschedule Operations
    scheduled_ops = 0
    makespan = 0
    schedule = []
    while(scheduled_ops < op_counter):
        for wp in workplaces:
            for i in range(len(workplaces[wp])):
                
                op = workplaces[wp][i]
                op_mach_pred_end = 0
                op_job_pred_end = 0
                op_time_for_resources = 0
                setup_time = 0
                
                if (op.scheduled):
                    continue

                #check if machine pred is finished
                if (i > 0):
                    if (not workplaces[wp][i - 1].scheduled):
                        continue
                    else:
                        op_mach_pred_end = workplaces[wp][i - 1].end

                #check if job pred is finished
                if (len(op.preds) > 0):
                    if (not op.preds[0].scheduled):
                        continue
                    else:
                        op_job_pred_end = op.preds[0].end
                
                #check if there are enough resources to start the job
                if (op.isFirst()):
                    bom_is_good = True
                    for bome in mgr.products[op.job.product_id].required_products:
                        required_material_quantity = bome.multiplicity * op.job.quantity
                        time_for_bome = resource_state[bome.required_material.Id].hasQuantity(required_material_quantity)
                        if (time_for_bome < 0):
                            bom_is_good = False
                        else:
                            op_time_for_resources = max(op_time_for_resources, time_for_bome)
                    
                    if (not bom_is_good):
                        continue
                    
                #Calculate setup time if needed
                op_product = mgr.products[op.job.product_id]
                op_pf = mgr.productFamilies[op_product.ProductFamiliesId]

                line_type_id = mgr.floor.workplaces[wp].line.line_type.Id
                if (line_type_id in op_pf.setup_times.keys()):
                    setup_time = op_pf.setup_times[line_type_id]

                if (i > 0):
                    pred_product = mgr.products[workplaces[wp][i-1].job.product_id]
                    pred_pf = mgr.productFamilies[pred_product.ProductFamiliesId]
                    if (pred_pf.Id == op_pf.Id):
                        setup_time = 0
                        
                #Proceed with op scheduling
                op_setup_end = op_mach_pred_end + setup_time
                
                op.start = max(op_job_pred_end, op_setup_end, op_time_for_resources)
                op.end = op.start + op.proc_time
                
                #Accumulate resources
                if (op.isFirst()):
                    for bome in mgr.products[op.job.product_id].required_products:
                        required_material_quantity = bome.multiplicity * op.job.quantity
                        resource_state[bome.required_material.Id].removeQuantity(op.start, required_material_quantity)
                
                if (op.isLast()):
                    if not op.job.isOrderJob:
                        resource_state[op.job.product_id].addQuantity(op.end, op.job.quantity)
                    else:
                        produced_resources[op.job.product_id] += op.job.quantity
                
                scheduled_ops += 1
                op.scheduled = True
                makespan = max(makespan, op.end)
                schedule.append((op.job.order_id, op.job.product_id, wp, op.start, op.end))
                
                t = [x for x in sol_json['Schedule'] if x['OrderId'] == op.job.order_id and x['WorkplaceID'] == wp]
                if (op.start != t[0]['Start']):
                    print("CHECK ORDER", op.job.order_id, "workplace", wp)
    
    schedule = sorted(schedule, key=lambda x: x[0])
    
   
    
    #Report remaining resource quantities
    #All should be 0
    for r in resource_state:
       if (resource_state[r].res_blocks[-1].quantity != 0):
           print("CHECK")
    
    #Check produced order requirements
    for p in order_requirements:
        if produced_resources[p] != order_requirements[p]:
            print("MISSING GOODS FROM PRODUCTION")

    print("Calculated Makespan ", makespan)
    if (makespan == sol_json['Metrics']['Makespan']):
        return True
    return False
    

def resource_tests():
    #Test resource blocks
    r = Resource()
    r.addQuantity(15, 100)
    r.addQuantity(15, 100)
    r.addQuantity(11, 3)
    r.addQuantity(11, 7)
    r.report()
    r.addQuantity(20, 100)
    r.report()
    print('-------------')
    tfor300 = r.hasQuantity(300)
    print("time for 300", tfor300)
    r.removeQuantity(25, 300)
    r.addQuantity(11, 10)
    r.report()

    print('Time that r has quantity 20:', r.hasQuantity(20))
    r.removeQuantity(11, 20)
    r.report()
    print('Time that r has quantity 10:', r.hasQuantity(10))

if __name__ == "__main__":
    sol_file = "test_run.json"
    f = open(sol_file,'r')
    sol_data = f.read()
    f.close()

    f = open("YOUR_FILENAME", "r")
    data = f.read()
    f.close()

    validate(json.loads(sol_data), json.loads(data)['data'])
